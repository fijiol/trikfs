/*
 * events.h
 *
 *  Created on: May 11, 2014
 *      Author: fijiol
 */

#ifndef TFS_EVENTS_H_
#define TFS_EVENTS_H_

#include <tfs/config.h>
#include <tfs/utils.h>

#if CONFIG_EVENT_MIRRORING_ENABLED

typedef int tfs_mr_device_id_type;
//#define tfs_mr_device_id_type int

void tfs_mr_create_node(
		const char* path,
		size_t input_tuple_size,
		size_t output_tuple_size,
		tfs_mr_device_id_type* mdi);

void tfs_mr_send_read_event(tfs_mr_device_id_type mei, const char* buf, size_t size);

void tfs_mr_send_write_event(tfs_mr_device_id_type mei, const char* buf, size_t size);

void tfs_mr_release_node(tfs_mr_device_id_type mei);

int tfs_mr_read_from_buffer(char* buf, size_t size);

void tfs_mr_init();
void tfs_mr_tini();

#else /* empty stub */

#define tfs_mr_device_id_type int
#define tfs_mr_create_node(path, its, ots, mdi_ptr)
#define tfs_mr_send_read_event(mdi, buf, size) 0
#define tfs_mr_send_write_event(mdi, buf, size) 0
#define tfs_mr_release_node(mdi)
#define tfs_mr_read_from_buffer(buf, size) 0
#define tfs_mr_init()
#define tfs_mr_tini()


#endif



#endif /* TFS_EVENTS_H_ */
