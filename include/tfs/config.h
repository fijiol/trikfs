/*
 * config.h
 *
 *  Created on: Feb 19, 2014
 *      Author: fijiol
 */

#ifndef CONFIG_H_
#define CONFIG_H_


#define CONFIG_DIRECT_READ()               1
#define CONFIG_MULTITHREAD_LOGGER()        1
#define CONFIG_NEW_THREAD_FOR_READ_LOOP() !CONFIG_DIRECT_READ()
#define CONFIG_DONT_SKIP_DATA()            1
#define CONFIG_EVENT_MIRRORING_ENABLED     0
#define CONFIG_PARTIAL_DEVICE_PACKAGE      1

#define FUSE_USE_VERSION 30


#endif /* CONFIG_H_ */
