
#ifndef TFS_UTILS
#define TFS_UTILS

#include <luajit-2.0/lua.h>

int tfs_ls(lua_State* L, const char* path, void* common, void(*f)(void* common, const char* p));

#define TF_FT_NON_EXISTS -1
#define TF_FT_CHAR_DEVICE 1
#define TF_FT_DIRECTORY   2
#define TF_FT_EVENTS_FILE 3

int tfs_getfiletype(lua_State* L, const char* path);

const char* start_with(const char* sentence, const char* word);

int is_empty_str(const char* str);


/*
 * buffers manipulation framework
 */
#define DEFAULT_BUFFER_SIZE	16//4*1024

typedef struct { size_t size, entity_size, count; char *beg, *end; char buf_placeholder; } tfs_common_buf;

#define BUF(name) name##_tfs_b
#define BUFS(name)name##_tfs_s
#define BUF_STRUCT(name) name##_tfs_b_struct
#define BUFT(name) name##_tfs_b_t

//static ?!
#define DECLARE_BUFT(name, entity_size_, size_) \
	struct BUF_STRUCT(name) { size_t size, entity_size, count; char *beg, *end; char buf[size_]; }; \
	static const size_t BUFS(name) = size_;\
	typedef struct BUF_STRUCT(name) BUFT(name);

#define DECLARE_BUF(name) \
		BUFT(name) BUF(name);

#define REINIT_BUF2(name, entity_size_) \
		BUF(name).count = 0, \
		BUF(name).size = sizeof(BUF(name).buf), \
		BUF(name).entity_size = entity_size_, \
		BUF(name).beg = BUF(name).buf, \
		BUF(name).end = BUF(name).buf

#define CLEAR_BUF(name) \
		BUF(name).count = 0, \
		BUF(name).beg = BUF(name).buf, \
		BUF(name).end = BUF(name).buf


#define BUF_GET(name, dest) \
		/*(trikfs_debug("buf "#name" get "#dest), 0)*/\
		tfs_buf_get(&BUF(name), dest)

#define BUF_PUT(name, source) \
		/*(trikfs_debug("buf "#name" put "#source), 0)*/\
		tfs_buf_put(&BUF(name), source)

#define BUF_ISEMPTY(name) \
		/*(trikfs_debug("check is empty buf "#name), 0)*/\
		tfs_buf_isempty(&BUF(name))

#define BUF_ISFULL(name) \
		/*(trikfs_debug("check is full buf "#name), 0)*/\
		tfs_buf_isfull(&BUF(name))

#define BUF_DUMP(name, str) \
	tfs_buf_dump(str, &BUF(name))

int tfs_buf_get(void* buf_struct, char* destination);
int tfs_buf_put(void* buf_struct, const char* source);
int tfs_buf_isempty(void* buf_struct);
int tfs_buf_isfull(void* buf_struct);
int tfs_buf_dump(char* str, void* buf_struct);

int init_lua(lua_State** lua_state);
int tini_lua(lua_State** lua_state);



#endif
