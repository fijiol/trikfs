/*
 * config_file.h
 *
 *  Created on: Feb 14, 2014
 *      Author: fijiol
 */

#ifndef CONFIG_FILE_H_
#define CONFIG_FILE_H_

#include <fuse.h>
#include <luajit-2.0/lua.h>
#include <tfs/config.h>
#include <tfs/utils.h>
#include <tfs_mirror/events.h>

#define CONFIG_MAX_SIZE 1024*10
#define CONFIG_FILE "config"
#define CONFIG_PATH "/"CONFIG_FILE

#define EVENTS_FILE "events"
#define EVENTS_PATH "/"EVENTS_FILE

//inner presentation of opened files
struct trikfs_file {
	int(*open)(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi);
	int(*read) (struct trikfs_file* trf, const char *path, char *buf, size_t size,
			off_t offset, struct fuse_file_info *fi);
	int(*write)(struct trikfs_file* trf, const char *path, const char *buf, size_t size,
			off_t offset, struct fuse_file_info *fi);
	int(*truncate)(struct trikfs_file* trf, const char *path, off_t offset);
	int(*release)(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi);
	int(*poll)(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi,
			struct fuse_pollhandle *ph, unsigned *reventsp);
	void *data;
};

lua_State* get_lua_state();
int config_truncate(size_t size);
size_t get_config_size();
struct trikfs_file* get_config_file();
char* get_currect_config();


struct trikfs_file tfs_config_file;

DECLARE_BUFT(input, 1, DEFAULT_BUFFER_SIZE);
DECLARE_BUFT(output, 1, DEFAULT_BUFFER_SIZE);

struct device_data {
	char name[256];
	int sf;
	size_t input_tuple_size;
	size_t output_tuple_size;
	DECLARE_BUF(input);
	DECLARE_BUF(output);
	pthread_mutex_t input_m;
	pthread_mutex_t output_m;
	int is_alive;
	int is_ready;
	pthread_t thread;
	lua_State* l;
	struct converter_info {
		enum conv_t {CT_UNDEFINED, CT_LUA_FUNC, CT_SO_FUNC, CT_COMPILED_SO_FUNC} conv;
		int converter;
		int (*conv_f)(int a_s, void* a, int b_s, void* b);
		void* shared_lib;
	} converter_info;

	tfs_mr_device_id_type mirror_device_id;

	struct fuse_pollhandle *ph;
	unsigned  *reventsp;
	pthread_mutex_t ph_lock;
};

struct trikfs_file* allocate_device_file(const char* path);

struct trikfs_file* get_events_file();

#endif /* CONFIG_FILE_H_ */
