
#ifndef TFS_LOG
#define TFS_LOG

#include <libgen.h>
#include <luajit-2.0/lua.h>
#include <stdarg.h>
#include <stdio.h>
#include <tfs/config.h>


/* debug macros */
#if 1
//#define TRIK_DEBUG
#define LOG_FILE "/home/fijiol/koning/trikfs/all.log"
#endif

void trikfs_efvprintf(int ec, FILE* stream, const char* keyword, const char* format, va_list arg);


void trikfs_evprintf(int ec, FILE* stream, const char* keyword, const char* format, va_list arg);

void trikfs_panic(const char* format, ...);

void trikfs_warning(const char* format, ...);

void trikfs_error(const char* format, ...);

void trikfs_info(const char* format, ...);

#ifdef TRIK_DEBUG
void trikfs_debug(const char* format, ...);
#else
#define trikfs_debug(...)
#endif

void trikfs_logl(const char* format, ...);

int trikfs_lua_log(lua_State *L);

int trikfs_lua_printvalue(lua_State *L);

int trikfs_lua_printtable(lua_State *L);

int trikfs_lua_printvalue(lua_State *L);

int trikfs_lua_printstack(lua_State *L);



char* buf2str(char* str, size_t str_s, const char* buf, size_t buf_s);

#ifdef TRIK_DEBUG
#define ASSERT_EXIT
#define tr_assert(b, s, ...) (!(b) ? \
		(trikfs_debug("assertion (" #b ") failed in " __FILE__ " : %d : " s, __LINE__, __VA_ARGS__), exit(0)) : 0 )
#define tr_assert2(b, s) tr_assert(b, s, "")
#else
#define tr_assert(...)
#define tr_assert2(...)
#endif


#endif /* TFS_LOG */
