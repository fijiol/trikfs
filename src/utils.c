
/*
 * propose of this file
 */

#include <string.h>
#include <unistd.h>
#include <luajit-2.0/lauxlib.h>
#include <luajit-2.0/lua.h>
#include <luajit-2.0/lualib.h>
#include <tfs/log.h>
#include <tfs/utils.h>

/*
 * if success returns pointer to tail string elsewhere returns NULL
 */
const char* start_with(const char* sentence, const char* word) {
	trikfs_debug("start_with (%s,%s)", sentence, word);

	if (NULL == sentence) {
		return NULL;
	}

	for(; *sentence != '\0' && *word != '\0' && *sentence == *word; ++sentence, ++word);

	if (*word == '\0') {
		trikfs_debug("start_with returns (%s)", sentence);
		return sentence;
	}

	trikfs_debug("start_with returns (NULL)");
	return NULL;
}

int is_empty_str(const char* str){
	return NULL != str && '\0' == *str;
}

int tfs_ls(lua_State* L, const char* path, void* common, void(*f)(void* common, const char* p)) {
	trikfs_debug("tfs_ls (%s) lua_state = %p", path, L);

	lua_getglobal(L, "trikfs"); //global.trikfs

	if (lua_isnil(L, -1)){ //there is no global.trikfs
		lua_pop(L, 1);
		if (!strcmp("/", path)) {
			return 0;
		} else {
			return -1;
		}
	}

	if (!strcmp("/", path)) {
		lua_pushnil(L);
		while (lua_next(L, -2)) {
			(*f)(common, lua_tostring(L, -2));
			lua_pop(L, 1);
		}
		return 0;
	}

	lua_pushnil(L);
	while (lua_next(L, -2)) {
		const char* subdir =  lua_tostring(L, -2);

		if (is_empty_str(start_with(start_with(path ,"/"), subdir))) {
			// this is some virtual device!

			/*
			 * Maybe you will want do some work with lua presentation of virtual device
			 *
			 * lua_pushvalue(L, -1);
			 * lua_pop(L, 1);
			 */

			// Add `device` file that actually present virtual device inside trikfs
			(*f)(common, "device");
			return 0;
		}

		lua_pop(L, 1);
	}

	lua_pop(L, 1); //global.trikfs

	return -1;
}

int tfs_getfiletype(lua_State* L, const char* path) {
	trikfs_debug("tfs_getfiletype (%s) lua_state = %p", path, L);

	lua_getglobal(L, "trikfs"); //global.trikfs

	if (lua_isnil(L, -1)){ //there is no global.trikfs
		lua_pop(L, 1);
		return -1;
	}

	lua_pushnil(L);
	while (lua_next(L, -2)) {
		const char* subdir =  lua_tostring(L, -2);

		if (is_empty_str(start_with(start_with(path ,"/"), subdir))) {
			return TF_FT_DIRECTORY;
		}

		if (is_empty_str(start_with(start_with(start_with(start_with(path ,"/"), subdir), "/"), "device"))) {
			return TF_FT_CHAR_DEVICE;
		}

		lua_pop(L, 1);
	}

	lua_pop(L, 1); //global.trikfs

	return TF_FT_NON_EXISTS;
}

/*
 * buffers manipulation routines
 */
int tfs_buf_get(void* buf_struct, char* destination) {
	size_t sequence_size = ((tfs_common_buf*)buf_struct)->entity_size;
	size_t buf_size = ((tfs_common_buf*)buf_struct)->size;

	char* buf = &((tfs_common_buf*)buf_struct)->buf_placeholder;
	char** beg = &((tfs_common_buf*)buf_struct)->beg;

	if (tfs_buf_isempty(buf_struct)) {
		return -1;
	}

	if (destination) {
		for (size_t i = 0; i<sequence_size; ++i) {
			*destination = **beg;

			++destination; ++*beg;

			if (*beg - buf >= buf_size) {
				*beg = buf;
			}
		}
	} else { //when destination == NULL, simply remove element without copying to somewhere
		*beg += sequence_size;
		if (*beg - buf >= buf_size) {
			*beg -= buf_size;
		}
	}

	--((tfs_common_buf*)buf_struct)->count;

	return 0;
}

int tfs_buf_put(void* buf_struct, const char* source) {
	size_t sequence_size = ((tfs_common_buf*)buf_struct)->entity_size;
	size_t buf_size = ((tfs_common_buf*)buf_struct)->size;

	char* buf = &((tfs_common_buf*)buf_struct)->buf_placeholder;
	char** end = &((tfs_common_buf*)buf_struct)->end;

	if (tfs_buf_isfull(buf_struct)) {
		return -1;
	}
	for (size_t i = 0; i<sequence_size; ++i) {
		**end = *source;

		++*end; ++source;

		if (*end - buf >= buf_size) {
			*end = buf;
		}
	}

	++((tfs_common_buf*)buf_struct)->count;

	return 0;
}

int tfs_buf_isempty(void* buf_struct) {
	return ((tfs_common_buf*) buf_struct)->count == 0;
}

int tfs_buf_isfull(void* buf_struct) {
	size_t sequence_size = ((tfs_common_buf*)buf_struct)->entity_size;
	size_t buf_size = ((tfs_common_buf*)buf_struct)->size;

	return (((tfs_common_buf*)buf_struct)->count+1) * sequence_size > buf_size;
}

int tfs_buf_dump(char* str, void* buf_struct) {
//	trikfs_debug("tfs_buf_dump %p %p", str, buf_struct);

	size_t sequence_size = ((tfs_common_buf*)buf_struct)->entity_size;
	size_t buf_size = ((tfs_common_buf*)buf_struct)->size;

	tfs_common_buf* tcb = (tfs_common_buf*)buf_struct;

//	trikfs_debug("size=%d", buf_size);
//	trikfs_debug("size=%ld, es=%ld, count=%ld", buf_size, sequence_size, tcb->count);

	str += sprintf(str, "size=%ld, es=%ld, count=%ld\n", buf_size, sequence_size, tcb->count);

	str += sprintf(str, "mem=[");
//	trikfs_debug("mem=[");
	for (int i=0;i<buf_size;++i) {
		str += sprintf(str, "0x%x, ", ((char*)&tcb->buf_placeholder)[i]);
//		trikfs_debug("0x%x, ", ((char*)&tcb->buf_placeholder)[i]);
	}
	str += sprintf(str, "],\n""offset=%ld,\n""tuples=", tcb->beg - &tcb->buf_placeholder);
//	trikfs_debug("],\n""offset=%ld,\n""tuples=", tcb->beg - &tcb->buf_placeholder);
	char *beg = tcb->beg;
	for (int i=0;i<tcb->count;++i) {
		str += sprintf(str, "(");
//		trikfs_debug("(");
		for (int j=0;j<sequence_size;++j) {
			str += sprintf(str, "0x%x,", *beg);
//			trikfs_debug("0x%x,", *beg);
			++beg;
			if (beg-tcb->beg >= buf_size) {
				beg = tcb->beg;
			}
		}
		str += sprintf(str, "), ");
//		trikfs_debug("), ");
	}

	return 0;
}


int init_lua(lua_State** lua_state) {
	trikfs_info("Initialize new LUA VM");
	*lua_state = luaL_newstate();
	luaL_openlibs(*lua_state);

	//TODO
	char path[PATH_MAX];
	char* pp = getcwd(path, PATH_MAX);
	trikfs_debug("working directory while init (%s)", pp);
	//TODO

	lua_register(*lua_state, "trikfs_log", trikfs_lua_log);
	lua_register(*lua_state, "trikfs_log_table", trikfs_lua_printtable);

	//TODO fix path
	if (luaL_dofile(*lua_state, "/home/fijiol/koning/trikfs/prelude.lua")) {
		trikfs_error("Failed to load prelude.lua (%s)", lua_tostring(*lua_state, -1));
		return -1;
	}

	return 0;
}

int tini_lua(lua_State** lua_state) {
	trikfs_info("Destroy LUA VM");
	lua_close(*lua_state);
	lua_state = NULL;

	return 0;
}

