
#include <tfs/config.h>

#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <tfs/log.h>

static int log_counter = 0;

#if CONFIG_MULTITHREAD_LOGGER()
static pthread_mutex_t log_m;
#endif

void trikfs_efvprintf(int ec, FILE* stream, const char* keyword, const char* format, va_list arg) {
#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_lock(&log_m);
#endif
    fprintf(stream, "[%s] ", keyword);
#if CONFIG_MULTITHREAD_LOGGER()
    fprintf(stream, "{%x} ", (unsigned int)pthread_self());
#endif
    fprintf(stream, "(%d) ", log_counter++);
    vfprintf(stream, format, arg);
    fprintf(stream, "\n");
    if (ec) {
        exit(ec);
    }
#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_unlock(&log_m);
#endif
}

void trikfs_evprintf(int ec, FILE* stream, const char* keyword, const char* format, va_list arg) {
#ifndef LOG_FILE
    trikfs_efvprintf(0, stream, keyword, format, arg);
#else
    FILE *file = fopen(LOG_FILE, "a");
    trikfs_efvprintf(ec, file, keyword, format, arg);
    fclose(file);
#endif
}

void trikfs_panic(const char* format, ...) {
    va_list args;
    va_start(args, format);
    trikfs_evprintf(1, stderr, "panic", format, args);
    va_end(args);
}

void trikfs_warning(const char* format, ...) {
    va_list args;
    va_start(args, format);
    trikfs_evprintf(0, stderr, "warning", format, args);
    va_end(args);
}

void trikfs_error(const char* format, ...) {
    va_list args;
    va_start(args, format);
    trikfs_evprintf(0, stderr, "error", format, args);
    va_end(args);
}

void trikfs_info(const char* format, ...) {
    va_list args;
    va_start(args, format);
    trikfs_evprintf(0, stdout, "info", format, args);
    va_end(args);
}

#ifdef TRIK_DEBUG

void trikfs_debug(const char* format, ...) {
    va_list args;
    va_start(args, format);
    trikfs_evprintf(0, stdout, "debug", format, args);
    va_end(args);
}
#else
#define trikfs_debug(...)
#endif

static int ll_level = 0;
static char* ll_keyword = "logl";
void trikfs_logl(const char* format, ...) {
    va_list args;
    va_start(args, format);
    char buf[1024];
    int i;
    for (i=0;i<ll_level;++i) {
        buf[i] = '\t';
    }
    strcpy(&buf[i], format);
    trikfs_evprintf(0, stdout, ll_keyword, &buf[0], args);
    va_end(args);
}

int trikfs_lua_log(lua_State *L) {
#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_lock(&log_m);
#endif
    FILE *file = fopen(LOG_FILE, "a");
    fprintf(file, "[Lua] ");

    int n = lua_gettop(L);
    int i;
    for (i = 1; i <= n; ++i) {
        fprintf(file, "%s", lua_tostring(L, i));
    }

    fprintf(file, "\n");
    fclose(file);

#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_unlock(&log_m);
#endif

    return 0;
}

int trikfs_lua_printvalue(lua_State *L);
    
int trikfs_lua_printtable(lua_State *L) {
#define cur_printer ll_keyword = "lua_table", trikfs_logl
    cur_printer("table {");
    ++ll_level;
    lua_pushnil(L);
    while (lua_next(L, -2)) {
        lua_pushvalue(L, -2);
        trikfs_lua_printvalue(L);
        lua_pop(L, 1);
        cur_printer("  =  ");
        trikfs_lua_printvalue(L);
        cur_printer(",");
        
        lua_pop(L, 1);
    }
    --ll_level;
    cur_printer("}");
    
    return 0;
#undef trikfs_info
}

int trikfs_lua_printvalue(lua_State *L) {
#define cur_printer ll_keyword = "lua_table", trikfs_logl
    if (lua_isnil(L, -1)) {
        cur_printer("(nil)");
        return 0;
    }
    if (lua_isboolean(L, -1)) {
        cur_printer("boolean (%d)", lua_tonumber(L, -1));
        return 0;
    }
    if (lua_isfunction(L, -1)) {
        cur_printer("<function>");
        return 0;
    }
    if (lua_islightuserdata(L, -1)) {
        cur_printer("<user_data>");
        return 0;
    }
    if (lua_isthread(L, -1)) {
        cur_printer("<thread>");
        return 0;
    }
    if (lua_isstring(L, -1)) {
        cur_printer("string (%s)", lua_tostring(L, -1));
        return 0;
    }
    if (lua_istable(L, -1)) {
        return trikfs_lua_printtable(L);
    }
    //TODO lua_pushstring ("error description")
    return 1; //unknown type
#undef cur_printer
}

int trikfs_lua_printstack(lua_State *L) {
//    lua_Debug info;
//    int level = 0;
    trikfs_info("lua stack dump -%d-----------------------", lua_gettop(L));
//    while (lua_getstack(L, level, &info)) {
//        lua_getinfo(L, "nSl", &info);
//        trikfs_info("  [%d] %s:%d -- %s [%s]",
//                level, info.short_src, info.currentline,
//                (info.name ? info.name : "<unknown>"), info.what);
//        ++level;
//    }
    for (int i = 1; i <= lua_gettop(L); ++i) {
        lua_pushvalue(L, i);
        ll_keyword = "lua value", trikfs_logl("*--(%d)--", i);
        ++ll_level;
        trikfs_lua_printvalue(L);
        --ll_level;
        lua_pop(L, 1);
    }

    return 0;
}

char* buf2str(char* str, size_t str_s, const char* buf, size_t buf_s) {
	if (buf_s > str_s) {
		return "(increase buf size!)";
	}

	memcpy(str, buf, buf_s);
	str[buf_s] = '\0';

	return str;
}

int logger_init() {
#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_init(&log_m, NULL);
#endif
	return 0;
}

int logger_destroy() {
#if CONFIG_MULTITHREAD_LOGGER()
	pthread_mutex_destroy(&log_m);
#endif
	return 0;
}
