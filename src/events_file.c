
#include <tfs/config.h>

#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include <luajit-2.0/lauxlib.h>
#include <tfs/file.h>
#include <tfs/log.h>
#include <tfs_mirror/events.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

struct events_data {
} events_data = {};

lua_State* lua_state;

int f_events_open(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
	//TODO: Send here first hello messages (with names of existing files)
	trikfs_debug("open /events file");
	return 0;
}

int f_events_read(struct trikfs_file* trf, const char *path, char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {

	return tfs_mr_read_from_buffer(buf, size);
}

int f_events_write(struct trikfs_file* trf, const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	return 0;
}

int f_events_truncate(struct trikfs_file* trf, const char *path, off_t offset) {
	return 0;
}

int f_events_release(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
	return 0;
}

int f_events_poll(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi,
		struct fuse_pollhandle *ph, unsigned *reventsp) {
	return 0;
}


struct trikfs_file tfs_EVENTS_FILE = {
		.open = f_events_open,
		.read = f_events_read,
		.write = f_events_write,
		.truncate = f_events_truncate,
		.release = f_events_release,
		.poll = f_events_poll,
		.data = &events_data,
};

struct trikfs_file* get_events_file() {
	return &tfs_EVENTS_FILE;
}


