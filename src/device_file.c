
#include <tfs/config.h>

#include <errno.h>
#include <fcntl.h>
#include <fuse/fuse.h>
#include <luajit-2.0/lua.h>
#include <luajit-2.0/lauxlib.h>
#include <luajit-2.0/lualib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <dlfcn.h>
#include <poll.h>

#include <tfs/file.h>
#include <tfs/log.h>
#include <tfs/utils.h>

#if CONFIG_EVENT_MIRRORING_ENABLED
#include <tfs_mirror/events.h>
#endif

#define L get_lua_state()

#define MIN(a,b) ((a)<(b)?(a):(b))

#define LOOP_IN_BACKGROUND_THREAD 1

static int lib_id = 0;
struct trikfs_file tfs_device_file;

static void* trikfs_device_file__poll_producer(void* dd) {
	struct device_data *data = (struct device_data*) dd;
	struct pollfd pdf = { .fd = data->sf, .events = 0, .revents = 0 };
	while (data->is_alive) {
		pdf.revents = 0;
		pdf.events = POLLIN; //FIXME: tt;
		poll(&pdf, 1, 0);
		pthread_mutex_lock(&data->ph_lock);
		trikfs_info("poll handle=%x", data->ph);
		if (data->ph) {
			trikfs_info("notify poll handle=%x", data->ph);
//			*data->reventsp = pdf.revents;
			fuse_notify_poll(data->ph);
			fuse_pollhandle_destroy(data->ph);
		}
		pthread_mutex_unlock(&data->ph_lock);
		usleep(1);
	}
}

struct trikfs_file* allocate_device_file(const char* path) {
	trikfs_debug("allocate device file(%s)", path);
	struct trikfs_file* tf = malloc(sizeof(struct trikfs_file));
	memcpy(tf, &tfs_device_file, sizeof(struct trikfs_file));
	tf->data = malloc(sizeof(struct device_data));
	memset(tf->data, 0, sizeof(struct device_data));
	return tf;
}

int free_device_file(struct trikfs_file* df) {
	free(df->data);
	free(df);
	return 0;
}

char* tfs_lua_getfield(lua_State *L, const char* field) {
	lua_getfield(L, -1, field);

	if (lua_isnil(L, -1)) {
		lua_pop(L, 1);
		trikfs_error("Failed to get field(%s) form lua table", field);
		return NULL;
	}

	char* tr = lua_tostring(L, -1);
	lua_pop(L, 1);

	return tr;
}

static int compile_converter(struct device_data* data) {
	trikfs_debug("compile converter...");

	data->converter_info.conv = CT_UNDEFINED;

	trikfs_debug(".");

	lua_getglobal(data->l, "trikfs");
	trikfs_debug(".");
	if (lua_isnil(data->l, -1)) {
		trikfs_error("Failed to get /trikfs root from reading thread");
		return -1;
	}
	trikfs_debug(".");

	lua_getfield(data->l, -1, data->name);
	if (lua_isnil(data->l, -1)) {
		trikfs_error("Failed to get /trikfs/(%s) device data from reading thread", data->name);
		return -1;
	}
	trikfs_debug(".");

	lua_getfield(data->l, -1, "converter");
	if (lua_isnil(data->l, -1)) {
		trikfs_error("Converter isn't defined. Please fix config");
		return -1;
	}
	trikfs_debug(".");

	//Converter is lua function
	if (lua_isfunction(data->l, -1)) {
		trikfs_debug("lua func");

		data->converter_info.conv = CT_LUA_FUNC;
		data->converter_info.converter = luaL_ref(data->l, LUA_REGISTRYINDEX);
		trikfs_debug("has got converter(%d) id, device=%s, alive=%d, ins=%d, outs=%d",
				data->converter_info.converter, data->name, data->is_alive, data->input_tuple_size, data->output_tuple_size);
	}
	//Converter is embedded C function
	else if (lua_istable(data->l, -1)) {
		trikfs_debug("embedded C function");

		data->converter_info.conv = CT_COMPILED_SO_FUNC;

		lua_getfield(data->l, -1, "code");
		if (lua_isnil(data->l, -1)) {
			trikfs_error("There is no C code to compile.");
			return -1;
		}

		//prepare names
		++lib_id;
		char file_c[256]; sprintf(file_c, "/tmp/a%x.c", lib_id);
		char file_so[256]; sprintf(file_so, "/tmp/a%x.so", lib_id);
		char file_l[256]; sprintf(file_l, "/tmp/a%x.log", lib_id);

		trikfs_debug("Files to process: %s %s %s", file_c, file_so, file_l);

		//crete *.c file
		FILE *f = fopen(file_c, "w");
		if (!f) {
			trikfs_error("failed to create (%s) file", file_c);
			return -1;
		}
		fprintf(f, "%s", lua_tostring(data->l, -1));
		fclose(f);

		//compile library
		char cmd[PATH_MAX];
		sprintf("gcc -shared -std=gnu99 -o %s %s 2>&1 > %s", file_so, file_c, file_l);

		trikfs_info("call sh: %s", cmd);
		if (system(cmd)) {
			trikfs_error("failed to compile: %s", cmd);
			return -1;
		}

		//load library and get C function
		trikfs_info("Load library %s", file_so);
		data->converter_info.shared_lib = dlopen(file_so, RTLD_LAZY);
		if (!data->converter_info.shared_lib) {
			trikfs_error("Failed to load shared library (%s)", file_so);
			return -1;
		}

		data->converter_info.conv_f = dlsym(data->converter_info.shared_lib, "converter");
		if (!data->converter_info.conv_f) {
			trikfs_error("Failed to get converter inv(*f)(int a_s, void* a, int b_s, void *b) "
					"function from shared library (%s)", file_so);
			return -1;
		}

		lua_pop(data->l, -2);
	}
	//Converter is function from .so library
	else if (lua_isstring(data->l, -1)) {
		trikfs_debug("from .so file");

		data->converter_info.conv = CT_SO_FUNC;

		data->converter_info.shared_lib = dlopen(lua_tostring(data->l, -1), RTLD_LAZY);
		if (!data->converter_info.shared_lib) {
			trikfs_error("Failed to load shared library (%s)", lua_tostring(data->l, -1));
			return -1;
		}

		data->converter_info.conv_f = dlsym(data->converter_info.shared_lib, "converter");
		if (!data->converter_info.conv_f) {
			trikfs_error("Failed to get converter function from shared library (%s)", lua_tostring(data->l, -1));
			return -1;
		}
	}

	trikfs_debug("compiling successfull");
	return 0;
}

static ssize_t run_converter(struct device_data* data, char* in, char* out, ssize_t size) {
	trikfs_debug("run_converter");

	ssize_t result = -1;

	switch (data->converter_info.conv) {
	case CT_LUA_FUNC:

		lua_rawgeti(data->l, LUA_REGISTRYINDEX, data->converter_info.converter);
//			trikfs_error("Failed to get reference to converter (%s)", lua_tostring(data->l, -1));

		lua_pushinteger(data->l, size);
		lua_pushlightuserdata(data->l, in);
		lua_pushinteger(data->l, data->output_tuple_size);
		lua_pushlightuserdata(data->l, out);

#if (defined TRIKFS_DEBUG)
		trikfs_lua_printstack(data->l);
#endif
		trikfs_debug("before pcall");
		if (lua_pcall(data->l, 4, 1, 0) != 0) {
			trikfs_error("Failed to call lua converter (%s)", lua_tostring(data->l, -1));
		}
		if (!lua_isnumber(data->l, -1)) {
			trikfs_error("Result of running lua converter should be number!");
		}
		result = lua_tonumber(data->l, -1);
		lua_pop(L, 1);
		lua_pop(L, 4);
		trikfs_debug("after pcall");

		break;
	case CT_SO_FUNC:
	case CT_COMPILED_SO_FUNC:
		trikfs_debug("before conv_f");
		result = data->converter_info.conv_f(size, (void *) in, data->output_tuple_size, (void *) out);
		trikfs_debug("after conv_f");
		break;
	case CT_UNDEFINED:
		trikfs_error("Trying to run undefined converter");
		break;
	}

	return result;
}

void* tfsImpl_read_routine(void *arg) {
	trikfs_debug("from tfsImpl_read_routine");

#if 0
	struct device_data* data = (struct device_data*) arg;

	while (data->is_alive) {
		char in[data->input_tuple_size], out[data->output_tuple_size];
		size_t r = read(data->sf, in, data->input_tuple_size);

		trikfs_debug("(%ld) bytes has been read from device (%s)", r, data->name);

		if (r == 0) {
			//EOF
			trikfs_debug("<eof> on device (%s)", data->name);
			data->is_alive = 0;
			break;
		}

		if (data->input_tuple_size != r) {
			trikfs_error("Expected to read (%ld) bytes, but has got (%ld). TODO fix code for work with that correctly", data->input_tuple_size, r);
		}

		BUF_PUT(data->input, in);


		/* TODO copy to output */
		int put_r = 0;
		do {
			pthread_mutex_lock(&data->output_m);
			put_r = BUF_PUT(data->output, out);
			pthread_mutex_unlock(&data->output_m);
		} while (put_r);

		if (!put_r) {
			char str[20*1024];
			BUF_DUMP(data->input, str);
			trikfs_debug("Input buffer: %s", str);
			BUF_DUMP(data->output, str);
			trikfs_debug("Output buffer: %s", str);
		}

	}
#endif
	return 0;
}

int device_open(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
	trikfs_debug("device open(%s)", path);
	fi->fh = (uint64_t)trf;
	struct device_data* dd = (struct device_data*)(trf->data);

	trikfs_debug("device_open (%s) lua_state = %p", path, L);


	lua_getglobal(L, "trikfs"); //global.trikfs

	lua_pushnil(L);
	while (lua_next(L, -2)) {
		const char* subdir =  lua_tostring(L, -2);

		if (is_empty_str(start_with(start_with(start_with(start_with(path ,"/"), subdir), "/"), "device"))) {
			//-------------------//-------------------//-------------------//-------------------//-------------------
			lua_getfield(L, -1, "src");

			trikfs_debug("open for (%s) file (%s) flags=%o", path, lua_tostring(L, -1), fi->flags);

			if (lua_isnil(L, -1)) {
				lua_pop(L, 4);
				return -ENXIO;
			}

			char* fn = lua_tostring(L, -1);
			dd->sf = open(fn, fi->flags);
			lua_pop(L, 1);

			if (dd->sf < 0) {
				trikfs_error("failed to open file (%s) for device (%s)", fn, subdir);
				return -errno;
			}

			//-------------------//-------------------//-------------------//-------------------//-------------------
			lua_getfield(L, -1, "input_tuple_size");
			if (lua_isnil(L, -1)) {
				lua_pop(L, 4);
				trikfs_error("there is no definition `input_tuple_size` for device (%s)" ,subdir);
				return -ENXIO;
			}

			dd->input_tuple_size = (long int) lua_tointeger(L, -1);
			REINIT_BUF2(dd->input, dd->input_tuple_size);

			lua_pop(L, 1);


			//-------------------//-------------------//-------------------//-------------------//-------------------
			lua_getfield(L, -1, "output_tuple_size");

			if (lua_isnil(L, -1)) {
				lua_pop(L, 4);
				trikfs_error("there is no definition `output_tuple_size` for device (%s)" ,subdir);
				return -ENXIO;
			}

			dd->output_tuple_size = (long int) lua_tointeger(L, -1);
			REINIT_BUF2(dd->output, dd->output_tuple_size);

			lua_pop(L, 1);

			//-------------------//-------------------//-------------------//-------------------//-------------------
			lua_getfield(L, -1, "converter");

			if (lua_isnil(L, -1)) {
				lua_pop(L, 4);
				trikfs_error("Converter routine doesn't define for device (%s)", subdir);
				return -ENXIO;
			}

//			dd->converter = luaL_ref(L, LUA_REGISTRYINDEX);
			strcpy(dd->name, subdir);
			if (init_lua(&dd->l) || luaL_dostring(dd->l, get_currect_config())) {
				trikfs_error("Cannot init lua vm");
				return -ENAVAIL;
			}
			compile_converter(dd);

			dd->is_alive = 1;
#if CONFIG_NEW_THREAD_FOR_READ_LOOP()

			trikfs_debug("before mutexs init");
			//TODO check return values
			pthread_mutex_init(&dd->input_m, NULL);
			pthread_mutex_init(&dd->output_m, NULL);

			trikfs_debug("before pthread create");
			pthread_create(&dd->thread, NULL, tfsImpl_read_routine, dd);

			trikfs_debug("after pthread create");
#endif

			pthread_mutex_init(&dd->ph_lock, NULL);
			pthread_create(&dd->thread, NULL, trikfs_device_file__poll_producer, dd);

			lua_pop(L, 1);

			//-------------------//-------------------//-------------------//-------------------//-------------------
			lua_pop(L, 3);

			tfs_mr_create_node(path, dd->input_tuple_size, dd->output_tuple_size, &dd->mirror_device_id);

			return 0;
		}

		lua_pop(L, 1);
	}

	lua_pop(L, 1); //global.trikfs

	return -ENOENT;
}

int device_read(struct trikfs_file* trf, const char *path, char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	trikfs_debug("device_read (%s) offset=%ld, size=%ld", path, offset, size);
	struct trikfs_file* tf = (struct trikfs_file*)(fi->fh);
	struct device_data* dd = (struct device_data*)(tf->data);

	return read(dd->sf, buf, size);

#if CONFIG_DIRECT_READ()
	size = size/dd->output_tuple_size*dd->output_tuple_size;

	size = dd->output_tuple_size; //here is dirty hack!//

	tr_assert((size % (dd->output_tuple_size)) == 0, "data should be requested by any count of tuples"
			" with size (%d), but has requested size to read == %d", dd->output_tuple_size, size);

	const int buf_size = 1024;
	char buf_in[buf_size];
	char *buf_inner = buf_in;
	char *buf_outer = buf;

	//TODO debug
	ssize_t r = read(dd->sf, buf_in, MIN(buf_size, size/dd->output_tuple_size*dd->input_tuple_size));
	ssize_t tr = 0;

	if (r<0) {
		trikfs_debug("read failed, errno = ", errno);
		return -errno;
	}

	for (ssize_t i = 0; i+dd->input_tuple_size <= r; i+=dd->input_tuple_size ) {
		r -= dd->input_tuple_size;
		run_converter(dd, buf_inner, buf_outer, dd->input_tuple_size);
		buf_inner += dd->input_tuple_size;
		buf_outer += dd->output_tuple_size;
		tr += dd->output_tuple_size;
	}

#if CONFIG_FRACTIONAL_DEVICE_PACKAGE
	tr += run_converter(dd, buf_inner, buf_outer, r);
#endif

	tfs_mr_send_read_event(dd->mirror_device_id, buf, tr);
	return tr;
#endif

#if CONFIG_NEW_THREAD_FOR_READ_LOOP()
	size_t s = 0;
	size_t es = BUF(dd->output).entity_size;

	size = es; //TODO ?!?!

	while (dd->is_alive && size >= es) {
		pthread_mutex_lock(&dd->output_m);
		while (size >= es && !BUF_ISEMPTY(dd->output)) {
			trikfs_debug("trik call... read(%p, %ld)", buf, size);
			size -= es;
			s += es;
			trikfs_debug("before buf-get s=(%d)", s);
			BUF_GET(dd->output, buf);
			trikfs_debug("after buf-get");
			buf += es;
			trikfs_debug("buf shift pointer");
		}
		pthread_mutex_unlock(&dd->output_m);
		//yield
		sleep(0);
		trikfs_debug("end of root loop");
	}

	trikfs_debug("device_read (%s) has been read size=%ld", path, s);

	tr_assert( s != 0, "s == 0 equals eof, if we return it here... "
			"need to check that it actually was that we meand");

	tfs_mr_send_read_event(dd->mirror_device_id, buf, s);
	return s;

#endif

	return 0;
}

int device_write(struct trikfs_file* trf, const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	trikfs_debug("device_write (%s) offset=%ld, size=%ld", path, offset, size);
	struct trikfs_file* tf = (struct trikfs_file*)(fi->fh);
	struct device_data* dd = (struct device_data*)(tf->data);

	trikfs_debug("sys call... write(%d, %p, %ld)", dd->sf, buf, size);
	ssize_t r = write(dd->sf, buf, size);
	trikfs_debug("%ld=write(%d, %p, %ld)", r, dd->sf, buf, size);
	if (r<0) {
		trikfs_debug("write failed, errno = ", errno);
		return -errno;
	}

	tfs_mr_send_write_event(dd->mirror_device_id, buf, r);
	return r;
}

int device_truncate(struct trikfs_file* trf, const char *path, off_t offset) {
	trikfs_debug("device_truncate(%s) offset=%ld", path, offset);
	return 0;
}

int device_release(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
	trikfs_debug("device_release(%s)", path);
	struct trikfs_file* tf = (struct trikfs_file*)(fi->fh);
	struct device_data* dd = (struct device_data*)(tf->data);

	tfs_mr_release_node(dd->mirror_device_id);

	dd->is_alive = 0;
#if 0

	pthread_join(dd->thread, NULL);
	pthread_mutex_destroy(&dd->input_m);
	pthread_mutex_destroy(&dd->output_m);
#endif
	pthread_join(dd->thread, NULL);
	pthread_mutex_destroy(&dd->ph_lock);

	close(dd->sf);
	tini_lua(&dd->l);

	free_device_file(tf);
	return 0;
}

int device_poll(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi,
		struct fuse_pollhandle *ph, unsigned *reventsp) {
	struct trikfs_file* tf = (struct trikfs_file*)(fi->fh);
	struct device_data* dd = (struct device_data*)(tf->data);

	pthread_mutex_lock(&dd->ph_lock);
	struct fuse_pollhandle* old = dd->ph;
	if (old) {
		fuse_pollhandle_destroy(old);
	}
//	dd->reventsp = reventsp;
	dd->ph = ph;
	trikfs_info("set %s poll handle=%x", path, ph);
	pthread_mutex_unlock(&dd->ph_lock);

	*reventsp |= POLLIN;
}

struct trikfs_file tfs_device_file = {
		.open = device_open,
		.read = device_read,
		.write = device_write,
		.truncate = device_truncate,
		.release = device_release,
		.poll = device_poll,
};



