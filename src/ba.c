
int  get(void* buf, double pos) {
	return ((char*)buf)[(int)pos];
}

void set(void* buf, double pos, double val) {
	((char*)buf)[(int)pos] = val;
}

