/*
 * some license here
 * some nickname
 * some notes
 */

#define FUSE_USE_VERSION 30

#include <tfs/config.h>

#include <stdlib.h>
#include <libgen.h>

#include <poll.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include <stdio.h>
#include <luajit-2.0/lua.h>
#include <luajit-2.0/lauxlib.h>
#include <luajit-2.0/lualib.h>

#include <tfs/log.h>
#include <tfs/utils.h>
#include <tfs/file.h>

typedef uint64_t fh_t;
typedef int trikfs_file_type;

lua_State* lua_state = NULL;

//#define E0 , 0
#define E0



//FUSE methods
static int trikfs_getattr(const char *path, struct stat *stbuf) {
	trikfs_debug("getattr(%s)", path);
	memset(stbuf, 0, sizeof(struct stat));

	if (!strcmp("/", path)) {
		stbuf->st_mode = S_IFDIR | 0666;
		stbuf->st_nlink = 2;
		return 0;
	}

	if (!strcmp(CONFIG_PATH, path)) {
		stbuf->st_mode = S_IFREG | 0666;
		stbuf->st_nlink = 1;
		stbuf->st_size = get_config_size();
		return 0;
	}

	if (!strcmp(EVENTS_PATH, path)) {
		stbuf->st_mode = S_IFREG | 0666;
		stbuf->st_nlink = 1;
		return 0;
	}

	int ret = tfs_getfiletype(lua_state, path);

	/* Maybe some of them should be char device, block device and other? */
	switch (ret) {
	case TF_FT_CHAR_DEVICE:
		trikfs_debug("file(%s) is char device", path);
//		stbuf->st_mode = S_IFCHR | 0666;
		stbuf->st_mode = S_IFREG | 0666; //will create very big file now, instead char device, becaause it doesn't work properly now
		stbuf->st_nlink = 1;
//		stbuf->st_size = 1024*1024*1024; /* where it should be in production?? */
		break;
	case TF_FT_DIRECTORY:
		trikfs_debug("file(%s) is directory", path);
		stbuf->st_mode = S_IFDIR | 0666;
		stbuf->st_nlink = 2;
		break;
	case TF_FT_EVENTS_FILE:
	case TF_FT_NON_EXISTS:
		trikfs_debug("file(%s) doesn't exist", path);
		return -ENOENT;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int trikfs_opendir(const char *path, struct fuse_file_info *fi) {
	return 0;
}

/* readdir stub */
struct readdir_vclos {
	void* buf;
	fuse_fill_dir_t filler;
};

void readdir_iterator (void* vcl, const char* p) {
	struct readdir_vclos* v = ((struct readdir_vclos*) vcl);
	v->filler(v->buf, p, NULL, 0 E0);
	trikfs_debug("dir has (%s)", p);
}

static int trikfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		off_t offset, struct fuse_file_info *fi) {
	trikfs_debug("readdir(%s)", path);

	filler(buf, ".", NULL, 0 E0);
	filler(buf, "..", NULL, 0 E0);

	if (!strcmp("/", path)) {
		trikfs_debug("dir has (%s)", CONFIG_FILE);

		filler(buf, CONFIG_FILE, NULL, 0 E0);
	}

#if CONFIG_EVENT_MIRRORING_ENABLED
	if (!strcmp("/", path)) {
		trikfs_debug("dir has (%s)", CONFIG_FILE);

		filler(buf, EVENTS_FILE, NULL, 0 E0);
	}
#endif

	struct readdir_vclos rvc = { .buf = buf, .filler = filler };

	trikfs_debug("lua_state(%p)", lua_state);

	if (tfs_ls(lua_state, path, &rvc, readdir_iterator)) {
		return -ENOENT;
	}

	return 0;
}
/* --------------- */

static int trikfs_releasedir(const char *path, struct fuse_file_info *fi) {
	return 0;
}

static int trikfs_open(const char *path, struct fuse_file_info *fi) {
	trikfs_debug("open(%s)", path);
	if (!strcmp(CONFIG_PATH, path)) {
		fi->fh = (uint64_t) get_config_file();
		return get_config_file()->open(get_config_file(), path, fi);
	}

	if (!strcmp(EVENTS_PATH, path)) {
		fi->fh = (uint64_t) get_events_file();
		return get_events_file()->open(get_events_file(), path, fi);
	}

	fi->direct_io = 1; // TODO work with it

	struct trikfs_file* adf = allocate_device_file(path);
	trikfs_debug("adf=%p", adf);
	trikfs_debug("adf->open=%p", adf->open);
	return adf->open(adf, path, fi);
}

static int trikfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	trikfs_debug("trikfs_read(%s)", path);
	int r = 0;
//	while (0 >= (r = ((struct trikfs_file*)(fi->fh))->read(((struct trikfs_file*)(fi->fh)), path, buf, size, offset, fi))) {};
//	trikfs_info("readed %s %d bytes", path, r);
//	return r;
	return ((struct trikfs_file*)(fi->fh))->read(((struct trikfs_file*)(fi->fh)), path, buf, size, offset, fi);
}

static int trikfs_truncate(const char *path, off_t size) {
	if (!strcmp(CONFIG_PATH, path)) {
		return get_config_file()->truncate(get_config_file(), path, size);
	}
	//TODO truncate for others
	return 0;
}

static int trikfs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	return ((struct trikfs_file*)(fi->fh))->write(((struct trikfs_file*)(fi->fh)), path, buf, size, offset, fi);
}

static int trikfs_release(const char *path, struct fuse_file_info *fi) {
	int r = ((struct trikfs_file*)(fi->fh))->release(((struct trikfs_file*)(fi->fh)), path, fi);
	fi->fh = 0;
	return r;
}

int trikfs_poll(const char *path, struct fuse_file_info *fi, struct fuse_pollhandle *ph, unsigned *reventsp) {
	trikfs_debug("trikfs_poll(%s)", path);
	((struct trikfs_file*)(fi->fh))->poll(((struct trikfs_file*)(fi->fh)), path, fi, ph, reventsp);
	return 0;
}


static struct fuse_operations trikfs_ops = {
		.getattr = trikfs_getattr,
		.opendir = trikfs_opendir,
		.readdir = trikfs_readdir,
		.releasedir = trikfs_releasedir,
		.open = trikfs_open,
		.truncate = trikfs_truncate,
		.write = trikfs_write,
		.read = trikfs_read,
		.release = trikfs_release,
		.poll = trikfs_poll,
};


lua_State* get_lua_state() {
	return lua_state;
}


// --------------------

int main(int argc, char *argv[]) {
	//TODO: save here working directory! //need for .log and prelude.lua

	trikfs_info("hi, there");

	if (init_lua(&lua_state)) {
		trikfs_error("Error while initializing root lua vm");
	}

	tfs_mr_init();

	int ret = fuse_main(argc, argv, &trikfs_ops, NULL);

	tfs_mr_tini();

	trikfs_info("goodbye, dirty world");

	tini_lua(&lua_state);
	return ret;
}
