
#include <tfs/config.h>

#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include <luajit-2.0/lauxlib.h>
#include <tfs/file.h>
#include <tfs/log.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

struct config_data {
	size_t file_size;
	char buf[1024*10];
	int modified;
} config_data = { .modified = 0, .file_size = 0 };

lua_State* lua_state;

int f_config_open(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
//	struct config_data* cd = (struct config_data*) trf->data;
	return 0;
}

int f_config_read(struct trikfs_file* trf, const char *path, char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	struct config_data* cd = (struct config_data*) trf->data;

	if (offset + size > cd->file_size) {
		size = cd->file_size - offset;
	}

	memcpy(buf, &cd->buf[offset], size);

	return size;
}

int f_config_write(struct trikfs_file* trf, const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fi) {
	struct config_data* cd = (struct config_data*) trf->data;

	cd->file_size = MAX(cd->file_size, offset + size + 1);

	memcpy(cd->buf, buf, size);
	cd->buf[size] = '\0'; //TODO fix multithreading issue here
	cd->modified = 1;

	return size;
}

int f_config_truncate(struct trikfs_file* trf, const char *path, off_t offset) {
	struct config_data* cd = (struct config_data*) trf->data;
	cd->file_size = 0;
	return 0;
}

int f_config_release(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi) {
	struct config_data* cd = (struct config_data*) trf->data;

	if (cd->modified) {
		trikfs_debug("config content (%s)", cd->buf);
		if (luaL_dostring(get_lua_state(), cd->buf)) {
			trikfs_error("failed to read config: %s", lua_tostring(get_lua_state(), -1));
			lua_pop(get_lua_state(), 1);
		}
		cd->modified = 0;
	}
	return 0;
}

int f_config_poll(struct trikfs_file* trf, const char *path, struct fuse_file_info *fi,
		struct fuse_pollhandle *ph, unsigned *reventsp) {
	return 0;
}



struct trikfs_file tfs_CONFIG_FILE = {
		.open = f_config_open,
		.read = f_config_read,
		.write = f_config_write,
		.truncate = f_config_truncate,
		.release = f_config_release,
		.poll = f_config_poll,
		.data = &config_data,
};

int config_truncate(size_t size) {
	return tfs_CONFIG_FILE.truncate(&tfs_CONFIG_FILE, CONFIG_PATH, size);
}

size_t get_config_size() {
	return ((struct config_data*)tfs_CONFIG_FILE.data)->file_size;
}

struct trikfs_file* get_config_file() {
	return &tfs_CONFIG_FILE;
}

char* get_currect_config() {
	return config_data.buf;
}


