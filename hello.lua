
trikfs_log("Hi, there!")

trikfs = {
  random = {
    src = '/dev/urandom',
    input_tuple_size = 3,
    output_tuple_size = 3,
    converter = function (a_s, a, b_s, b) 
      --trikfs_log("hi, converter: ", a_s, " ", b_s)

      ba.set(b, 0, ba.get(a, 0) % 10 + 1 + 33)
      ba.set(b, 1, ba.get(a, 1) % 10 + 2 + 33)
      ba.set(b, 2, ba.get(a, 2) % 10 + 3 + 33)
      --ba.set(b, 3, ba.get(a, 3) % 10 + 4 + 33)
      return 3
    end
  },
  random2 = {
    src = '/dev/urandom',
    input_tuple_size = 3,
    output_tuple_size = 3,
    converter = id_create(3)
  },
	zero = {
		src = '/dev/zero',
		input_tuple_size = 4,
		output_tuple_size = 4,
		converter = {
			code = [[
				int converter(int in_s, char* in, int out_s, char* out) {
					out[0] = in[0] % 10 + '0' + 1;
					out[1] = in[1] % 10 + '0' + 2;
					out[2] = in[2] % 10 + '0' + 3;
					out[3] = in[3] % 10 + '0' + 4;
					return 4;
				}
			]],
		}
	},
  zero2 = {
    src = '/dev/zero',
    input_tuple_size = 4,
    output_tuple_size = 4,
    converter = '/home/fijiol/koning/trikfs/tmp.so',
    --converter = '/tmp/a001.so',
  },	

  mouse = {
	src = '/dev/input/mouse0',
	input_tuple_size = 4,
	output_tuple_size = 4,
	converter = '/home/fijiol/koning/trikfs/tmp.so'
  },

  keyboard = {
    src = '/dev/input/event2',
    input_tuple_size = 3,
    output_tuple_size = 3,
    converter = id_create(3)
  },
}

