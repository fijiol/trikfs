
trikfs_log("Before config")

wrapped_mouse1 = { 
	info = 'simple proxy device without any changes',
	dev = '/dev/input/mouse0',
	input_format = { {'dev', 'char'} } , -- create folder orig with these devices
	output_format = { 'char' },
	converter = function (a) 
		return a
	end
} 

wrapped_mouse2 = { 
	info = 'some string here! for example... this fake device that will get data from real mouse and convert it in some random maner!',
	dev1 = '/dev/input/mouse0',
	dev2 = '/dev/input/mouse1',
	-- available types should be all c types... probably with arrays like unsigned_char[13]
	-- something like:
	-- input_format = { 'dev1: signed char', 'dev2: unsigned char', 'dev1: int' } ,
	output_format = { 'char' },
	-- maybe /converter = "libblablabla.so"/ also good idea?
	converter = function (a,b,c) 
		return a
	end
} 

trikfs_add_device("wrapped_mouse1", wrapped_mouse1)
-- trikfs_add_device("wm2", wrapped_mouse2)
-- trikfs_remove_device
-- trikfs_update_device

trikfs_log("After config")

