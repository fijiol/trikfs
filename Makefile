
DIR=/trikfs
#DIR=/home/fijiol/trikfs
ifeq ($(CROSS),1)
$(info "cross 1")
PKG_CONFIG=-D_FILE_OFFSET_BITS=64 -I/usr/include/ -lfuse -L./fuse -I./luajit-2.0/src -lluajit -L./luajit-2.0/src
CCPARAMS=-std=gnu99 -I./include -lpthread -ldl $(PKG_CONFIG)
else
$(info "cross 0")
PKG_CONFIG=`pkg-config fuse --cflags --libs` `pkg-config luajit --cflags --libs` 
CCPARAMS=-std=gnu99 -Wall -g -O0 -I./include -lpthread -ldl $(PKG_CONFIG)
endif
CC?=./colorgcc/color-gcc


DD=#-d -f -s
VALGRIND=#valgrind
#-D_FILE_OFFSET_BITS=64 


PP=#sudo

all: ba id 
	$(CC) src/*.c $(CCPARAMS) -o trikfs

buffer:
	$(CC) src/log.c src/utils.c examples/buffer.c $(CCPARAMS) -o examples/buffer

ba:
	$(CC) $(CCPARAMS) src/ba.c -shared -o ./ba.so

id:
	$(CC) $(CCPARAMS) src/id.c -shared -o ./id.so

clone:
	echo TODO: write here some stuff to check out dependencies

install:
	echo install to host=$(HOST)
	ssh $(HOST) -- mkdir -p /home/fijiol/koning/trikfs
	scp *.sh *lua trikfs luajit-2.0/src/*.so* fuse/*.so* *.so*   $(HOST):/home/fijiol/koning/trikfs

run: um all
	rm -rf ./all.log
	touch ./all.log
	chmod 666 all.log
	$(PP) $(VALGRIND) ./trikfs $(DD) -o nonempty -o allow_other -odev $(DIR)
	sleep 1
	test -e /trikfs/config
	cat hello.lua > /trikfs/config
	test -e ./all.log && tail -f ./all.log || echo .

um:
	$(PP) killall -9 trikfs || echo .
	$(PP) fusermount -u $(DIR) || echo .
