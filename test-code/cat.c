
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

int main(int argc, char** argv) {
	for (int i=1; i<argc; ++i) {
		printf("file: %s\n", argv[i]);

		char buf[1024];

		int f = open(argv[i],  O_SYNC | O_NONBLOCK, O_RDONLY);
		if (f<=0) {
			printf("f=%d\n", f);
			printf("<error %s: %x, %s>\n", argv[i], -f, strerror(-f));
			printf("<error %s: %x, %s>\n", argv[i], errno, strerror(errno));
			return 1;
		}

		int r;
		//while ( (r = read(f, buf, 1024)) > 0 || 1) {
		while ( (r = read(f, buf, 16)) > 0 || 1) {
			printf("readed %d bytes\n", r);
			/* 
			continue;
			if (r == 0) {
				printf("\n<eof:%s>\n", argv[i]);
				//exit (1);
			}
			buf[r] = '\0';
			for (int j = 0;j<r; ++j) {
				putchar(buf[j]);
			}
			*/
		}

/*
		if (r<0) {
			printf("<error %s: %x, %s>\n", argv[i], errno, strerror(errno));
		}

		if (r==0) {
			printf("<eof:%s>\n", argv[i]);
		}
		*/

		close(f);
	}
	return 0;
}

