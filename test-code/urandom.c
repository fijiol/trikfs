
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main(void) {
	char buf[1024];
	//int f = open("/dev/urandom", 20120);
	int f = open("/dev/urandom", O_RDONLY);
	printf("f=%d\n", f);
	int r =0;
	while ( (r = read(f, buf, 1024)) > 0) {
		buf[r] = '\0';
		printf("readed %d bytes\n", r);
		for (int i = 0; i<r; ++i) {
			printf("%x, ", buf[i]);
		}
	}

	if (r<0) {
		printf("some error %x\n", -r);
	}

	if (r==0) {
		printf("end of file\n");
	}

	printf("r=%d\n", r);

	close(f);
	return 0;
}

